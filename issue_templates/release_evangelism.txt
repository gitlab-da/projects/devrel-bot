## 👀 Pins

* [DE Release Evangelism handbook](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/social-media/#release-evangelism) is the SSoT for release post MR, Slack URLs and other insights.

### ✍ Release post

* [Release Post MR](FIXME_MR)
* [Release Post Review App](FIXME_REVIEWAPP)

## 🔥 Interesting Items

### MVP

<!-- Create a screenshot from the MVP section, tag the user if possible, and give a shoutout, linking the blog post `#mvp` anchor. -->

### Plan/Create/Manage

<!-- Issues, epics, code review, editor, roadmaps, etc. -->

- [ ] Text:
  - URL:

### CI/CD 

<!-- CI config, editor, runner, deployments, environments, etc. -->

- [ ] Text:
  - URL:

### IaC/GitOps

<!-- Configure, Package, Release, Deploy, etc. -->

- [ ] Text:
  - URL:

### DevSecOps 

<!-- Secure, Protect, SAST, container/dependency scanning, etc. -->

- [ ] Text:
  - URL:

### Observability

<!-- Metrics, Opstrace, -->

- [ ] Text:
  - URL:

## ❤️ Social Shares

- Item:
  - Twitter:
  - LinkedIn: 

> 💡 Add a new buffer campaign and post screenshots into this issue. 

/label ~"dev-evangelism" ~"DE-Type::Evangelist" ~"DE-Status::ToDo" ~"DE-Bot::Auto"

