resource_rules:
  issues:
    rules:
      - name: Issues where DA team member is an assignee outside DA-Meta project i.e. DevRel-Influenced
        conditions:
          assignee_member:
            source: group
            condition: member_of
            source_id: 10087220
          state: opened
          ruby: get_project_id != 18266508 #https://gitlab.com/gitlab-com/marketing/developer-relations/developer-advocacy/developer-advocacy-meta
          forbidden_labels:
            - developer-advocacy
        actions:    
          labels:
            - developer-advocacy
            - DevRel-Influenced
            - DA-Bot::Skip
      - name: Issue by DA team member missing developer-advocacy label
        conditions:
          author_member:
            source: group
            condition: member_of
            source_id: 10087220
          state: opened
          forbidden_labels:
            - developer-advocacy
        actions:    
          labels:
            - developer-advocacy
      
      - name: Issue missing DA-Type label
        conditions:
          state: opened
          forbidden_labels:
            - DA-Bot::Skip
            - DA-Bot::Triage
            - DA-Type::Consulting
            - DA-Type::Events
            - DA-Type::Content
            - DA-Type::Process
            - DA-Type::Response
            - DA-Type::Evangelist
            - DA-Type::analysts
            - DA-Type::enablement
            - DA-Type::competitive
            - DA-Status::FYI
            - DA-Status::OnHold
          labels:
            - developer-advocacy
        actions:    
          labels:
            - DA-Bot::Triage
            - DA-Triage::no-issue-type
      - name: Content Issue missing DA-Type-Content label
        conditions:
          state: opened
          forbidden_labels:
            - DA-Bot::Skip
            - DA-Bot::Triage
            - DA-Type-Content::adoption
            - DA-Type-Content::blog
            - DA-Type-Content::cicd-component
            - DA-Type-Content::demo
            - DA-Type-Content::documentation
            - DA-Type-Content::event
            - DA-Type-Content::video
            - DA-Type-Content::newsletter
            - DA-Type-Content::keynote
            - DA-Type-Content::narrative
            - DA-Type-Content::product-tour
            - DA-Type-Content::quickstart
            - DA-Type-Content::talk
            - DA-Type-Content::tech-webinar
            - DA-Type-Content::tutorial
            - DA-Status::FYI
            - DA-Status::OnHold
            - CFP
          labels:
            - developer-advocacy
            - DA-Type::Content
        actions:    
          labels:
            - DA-Bot::Triage
            - DA-Triage::no-content-type
      - name: Set General Issue workflow to Todo when Content status is New
        conditions:
          state: opened
          forbidden_labels:
            - DA-Status::ToDo
          labels:
            - DA-Content::New
        actions:    
          labels:
            - DA-Status::ToDo
      - name: Set General Issue workflow to Doing when Content status is In-Progress
        conditions:
          state: opened
          forbidden_labels:
            - DA-Status::Doing
          labels:
            - DA-Content::In-Progress
        actions:    
          labels:
            - DA-Status::Doing
      - name: Set General Issue workflow to OnHold when Content status is Metrics is pending
        conditions:
          state: opened
          forbidden_labels:
            - DA-Status::OnHold
          labels:
            - DA-Content::Done-Metrics-Pending
        actions:    
          labels:
            - DA-Status::OnHold
      - name: Set General Issue workflow to OnHold when Content status is awaiting publication
        conditions:
          state: opened
          forbidden_labels:
            - DA-Status::OnHold
          labels:
            - DA-Content::Awaiting-Publication
        actions:    
          labels:
            - DA-Status::OnHold

      - name: Content Issue closed after collecting metrics, setting status to Published
        conditions:
          state: closed
          labels:
            - DA-Content::Done-Metrics-Pending
        actions:    
          labels:
            - DA-Content::Published
      - name: Set General Issue workflow to Done & close issue when Content status is Published
        conditions:
          state: opened
          forbidden_labels:
            - DA-Status::FYI
            - DA-Status::Done
            - DA-Bot::Skip
          labels:
            - DA-Content::Published
        actions:    
          labels:
            - DA-Status::Done
          status: close
          comment: |
              Issue closed because content has been published, if you want to keep it open while you collect metrics set the ~"DA-Content::Done-Metrics-Pending" label or `DA-Bot::Skip` for other reasons.
      - name: Content Issue missing content workflow status, set in-progress
        conditions:
          state: opened
          forbidden_labels:
            - DA-Content::New
            - DA-Content::Published
            - DA-Content::In-Review
            - DA-Content::In-Progress
            - DA-Content::Done-Metrics-Pending
            - DA-Content::Awaiting-Publication
          labels:
            - developer-advocacy
            - DA-Type::Content
        actions:    
          labels:
            - DA-Content::In-Progress
      - name: Missing Due Dates
        conditions:
          ruby: missing_due_date
          state: opened
          labels:
            - developer-advocacy
          forbidden_labels:
            - DA-Due::N/A
            - DA-Bot::Skip
            - DA-Status::FYI
            - DA-Status::OnHold
            - CFP
            - DA-Bot::Triage
        actions:
          labels:
            - DA-Bot-Auto-Due-Date
          comment: |
            /due #{get_current_quarter_last_date}
      - name: Issues On Hold
        conditions:
          ruby: due_date_past && Date.today - last_comment_at  > 90
          state: opened
          labels:
            - developer-advocacy
            - DA-Status::OnHold
          forbidden_labels:
            - DA-Bot::Skip
            - DA-Status::FYI
            - CFP
            - DA-Bot::Triage
            - DA-Status::FYI
        actions:
          comment: |
            Hey there! 
            
            **This issue has been on hold for more than 60 days, Do you want to keep it that way?**
            
            NB: You can use the `DA-Bot::Skip` label to make the DA-Bot ignore this issue or use the `DA-Status::FYI` to make the DA-Bot know that the DevAdvocacy team is only observing the issue and no action is required.
          labels:
            - DA-Bot::Triage
            - DA-Triage::onhold-too-long
      - name: Past Due Date & Previous due date was added by bot
        conditions:
          ruby: due_date_past
          state: opened
          forbidden_labels:
            - DA-Bot::Skip
            - DA-Status::FYI
            - DA-Status::OnHold
            - CFP
          labels:
            - developer-advocacy
            - DA-Bot-Auto-Due-Date
        actions:
          comment: |
            /due #{get_current_quarter_last_date}
      - name: Past Due Date
        conditions:
          ruby: due_date_past
          state: opened
          forbidden_labels:
            - DA-Bot::Triage
            - DA-Bot::Skip
            - DA-Status::FYI
            - DA-Status::OnHold
            - CFP
          labels:
            - developer-advocacy
        actions:
          labels:
            - DA-Bot::Triage
            - DA-Triage::past-due-date
      - name: No comment in 60 days
        conditions:
          ruby: due_date_past && Date.today - last_comment_at  > 60
          state: opened
          forbidden_labels:
            - DA-Bot::Skip
            - DA-Status::FYI
            - DA-Status::OnHold
            - CFP
            - DA-Bot::Triage
          labels:
            - developer-advocacy
        actions:
          comment: |
            Hey there! 
            
            **There has been no update on this issue in the last 60 days, is it done? You can apply the `DA-Status::Done` and close the issue. **
            
            NB: You can use the `DA-Bot::Skip` label to make the DA-Bot ignore this issue or use the `DA-Status::FYI` to make the DA-Bot know that the DevAdvocacy team is only observing the issue and no action is required.
          labels:
            - DA-Bot::Triage
            - DA-Triage::no-update-60days
      - name: Issue is done but still open
        conditions:
          state: opened
          forbidden_labels:
            - DA-Bot::Skip
            - DA-Status::FYI
            - DA-Status::OnHold
            - CFP
            - DA-Bot::Triage
          labels:
            - developer-advocacy
            - DA-Status::Done
        actions:
          comment: |
            Hey there! 
            
            **Awesome, now that the issue is 'Done', Are there any metrics we can document? If no further action is required, you can close the issue. I will also be putting any action from me on this issue on `DA-Status::OnHold`.**
            
            NB:You can use the `DA-Bot::Skip` label to make the DA-Bot ignore this issue or use the `DA-Status::FYI` to make the DA-Bot know that the DevAdvocacy team is only observing the issue and no action is required.
          labels:
            - DA-Bot::Triage
            - DA-Triage::done-not-closed
      - name: Close Old DA-Bot created Issues
        conditions:
          date:
            attribute: created_at
            condition: older_than
            interval_type: weeks
            interval: 2
          state: opened
          forbidden_labels:
            - DA-Bot::Skip
          labels:
            - DA-Bot::Auto
        actions:
          status: close
          comment: |
            Auto-created Issue closed automatically after 2 weeks of creation, if issue is still needed, kindly reopen and add the `DA-Bot::Skip`.
      - name: Missing Request Type Label for DA team members
        conditions:
          author_member:
            source: group
            condition: member_of
            source_id: 10087220
          state: opened
          forbidden_labels:
            - CFP
            - DA-Requester-Type::Internal
          labels:
            - developer-advocacy
        actions:    
          labels:
            - DA-Requester-Type::Internal
      - name: Missing Request Type Label for non DA team members
        conditions:
          author_member:
            source: group
            condition: not_member_of
            source_id: 10087220
          state: opened
          forbidden_labels:
            - CFP
            - DA-Requester-Type::External
          labels:
            - developer-advocacy
        actions:    
          labels:
            - DA-Requester-Type::External
      - name: Missing Requesting team label for DA-Consulting Label
        conditions:
          state: opened
          forbidden_labels:
            - DA-Bot::Skip
            - DA-Bot::Triage
            - DA-Consulting::Alliances
            - DA-Consulting::CorpComms
            - DA-Consulting::CorpEvents
            - DA-Consulting::Community
            - DA-Consulting::Engineering
            - DA-Consulting::FieldMktg
            - DA-Consulting::GrowthMktg
            - DA-Consulting::Product
            - DA-Consulting::Sales
            - DA-Consulting::DevRel
            - DA-Status::FYI
            - DA-Status::OnHold
          labels:
            - developer-advocacy
            - DA-Type::Consulting
        actions:    
          labels:
            - DA-Bot::Triage
            - DA-Triage::no-consulting-team

      - name: CFP Issues Missing due date
        conditions:
          ruby: missing_due_date
          state: opened
          labels:
            - CFP
          forbidden_labels:
            - DA-Bot::Skip
            - DA-Bot::Triage
            - DA-Status::FYI
            - DA-Status::OnHold
        actions:
          labels:
            - DA-Bot::Triage
            - DA-Triage::no-due-date
      
      - name: CFP Past Due date for submission
        conditions:
          ruby: due_date_past
          state: opened
          labels:
            - CFP
            - CFP::Open
          forbidden_labels:
            - DA-Bot::Skip
            - DA-Bot::Triage
            - DA-Status::FYI
            - DA-Status::OnHold
        actions:
          labels:
            - DA-Bot::Triage
            - DA-Triage::cfp-due-submission
            
      - name: CFP Past Due date for Notification
        conditions:
          ruby: due_date_past
          state: opened
          labels:
            - CFP
            - CFP::Submitted
          forbidden_labels:
            - DA-Bot::Skip
            - DA-Bot::Triage
            - DA-Status::FYI
            - DA-Status::OnHold
        actions:
          labels:
            - DA-Bot::Triage
            - DA-Triage::cfp-due-notification
      - name: CFP Past Due date for Presentation
        conditions:
          ruby: due_date_past
          state: opened
          labels:
            - CFP
            - CFP::Accepted
          forbidden_labels:
            - DA-Bot::Skip
            - DA-Bot::Triage
            - DA-Status::FYI
            - DA-Status::OnHold
        actions:
          labels:
            - DA-Bot::Triage
            - DA-Triage::cfp-due-presentation
